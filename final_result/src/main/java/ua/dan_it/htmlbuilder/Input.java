package ua.dan_it.htmlbuilder;


public class Input extends AbstractElement implements Element {

  protected Input() {
    super("input");
  }
}
