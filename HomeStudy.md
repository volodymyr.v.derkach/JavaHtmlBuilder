# Before the Classroom Tutorial. 

1. Install [Maven](https://maven.apache.org/install.html) on your machine. Command `$ mvn -v` to check. (10 min)
2. Try command `$ mvn archetype:generate` and check waht it does. (5 min)
3. Install `tree` command-line tool and play with it. (5 min)
4. Install [Java SDK](http://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html). Command `$ javac -v` should work. (15 min)
5. Let's write down simple Java application from command-line. (20 min)
    * Create a file `Hello.java` by `$ touch Hello.java`.
    * Open it in `vi` or `vim` editor. `$ vi Hello.java`.
    * Press `i` for insert mode.
    * type the following text
    
    ```java
      public class Hello {
        public static void main(String[] args) {
            System.out.println("Hello Dan.IT!");
        }
    }
    ```
    * Press `esc` to exit from insert mode.
    * Press `:` for command mode. Type `wq` in order to write changes and quit.
    * Execute `$ javac Hello.java`. You should get compiled `Hello.class` file. Try `$ ls` to check.
    * Run your application with `$ java Hello`. It should print in console `Hello DAN.IT!`.
6. Please read offisial Oracle documentation [here](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/index.html)