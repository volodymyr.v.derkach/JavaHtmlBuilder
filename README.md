# HTML Builder
In this lesson we going to implement java library aimed to ganerate HTML code.

###### Dependencies    
Just before you get started, familiarize yourself with following prerequisites.  
1. [Java SDK](http://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html) should be installed. Command `$ javac -v` should work.
2. You should have [Maven](https://maven.apache.org/install.html) installed on your machine. Command `$ mvn -v` to check.
3. You should have very basic understanding of Java language.

---

###### Lesson Goals  
1. Familiarize with __maven__ tool.
2. Get practice with TDD development methodology.
3. Get a better understanding of __Java__, encapsulation, inheritance, polymorphism, factory method DP.
4. Hot keys in IDE.

###### Format  
3 hours of work in classroom.
Teacher should follow __Copied Behaviour__ methodology.
Basically, livecoding with explenations.
Students should copy all code from projector.

---
1. Let's create a maven project first. In command line execute:

    `$ mvn archetype:generate`  
    Maven will ask you to choose project template that you want to extend.  
    `Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): 966:`  
    Press _Enter_ (default option will be selected)
    
    Then mavan want you to choose version of template project  
    ```
    Choose org.apache.maven.archetypes:maven-archetype-quickstart version: 
    1: 1.0-alpha-1
    2: 1.0-alpha-2
    3: 1.0-alpha-3
    4: 1.0-alpha-4
    5: 1.0
    6: 1.1
    Choose a number: 6: 
    ```

    Press _Enter_ (default option will be selected)
    
    Few more questions about your new project  
    
    `Define value for property 'groupId': : ` type `ua.dan-it`

    `Define value for property 'artifactId': :` type `html-builder`

    `Define value for property 'version':  1.0-SNAPSHOT: : `
    Press _Enter_ (default option will be selected)

    Define value for property 'package':  ua.dan-it: : 
    Type `ua.dan_it.htmlbuilder`
    
    ```
    Confirm properties configuration:
    groupId: ua.dan-it
    artifactId: html-builder
    version: 1.0-SNAPSHOT
    package: ua.dan_it.htmlbuilder
     Y: : 
    ```
    
2. The directory structure should look like this

    ```
    .
    └── html-builder
        ├── pom.xml
        └── src
            ├── main
            │   └── java
            │       └── ua
            │           └── dan_it
            │               └── htmlbuilder
            │                   └── App.java
            └── test
                └── java
                    └── ua
                        └── dan_it
                            └── htmlbuilder
                                └── AppTest.java
    
    12 directories, 3 files

    ```

    Check it out by `tree` command-line tool.
3. Go inside `html-builder` directory, `$ cd html-builder`.
4. Generated files __App.java__ and __AppTest.java__ are not needed, so you can remove it.
5. We will follow TDD methodology, so it's required to have __JUnit__ in our dependencies. Let's add it in __pom.xml__
    ```xml
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
    </dependency>
    ```
6. Rebuild the project `$ mvn clean package`. Now you should have __JUnit__ in your classpath[^1].
7. In the file `src/test/java/ua/dan_it/htmlbuilder/HtmlTest.java`, add new test case  
   
    ```java
    @Test
    public void emptyHtmlTag() {
      String html = new Html().print();
      assertThat(html, is("<html></html>"));
    }
    ```
8. According to TDD you should fix compilation errors. Create __Html__ class in __ua.dan_it.htmlbuilder__ pachage, and add  __print()__ method.
9. Run the test. It must fail!
10. Return value in __print()__ method you can leave hard-coded for now.
11. Now test should pass!
12. Let's move on. Next test.
    ```java
    @Test
    public void buttonElement() {
      String html = new Html()
        .add(new Button())
        .print();
      assertThat(html, is("<html><button></button></html>"));
    }
    ```
13. Again. We need to fix compilation errors. Let's create __Button__ class and __add()__ method inside __Html__.
14. Let's run our test. It must fail!
15. Now we need to add implementation. It can be quick and stupid solution just to make test pass. E.g.:
    
    in Button.java  
    ```java
    public String print() {  
      return "<button></button>";
    }
    ```
    
    in Html.java  
    ```java
    public Html add(Button button) {  
      this.button = button;
      return button;
    }
    
    public String print() {  
      return String.format("<html>%s</html>", button.print());
    }
    ```
    
16. Run the test. It should pass!
17. Let's add few more inner elements inside __Html__ object.
    ```java
    @Test
    public void elements() {
      String html = new Html()
          .add(new Button())
          .add(new Input())
          .print();
      assertThat(html, is("<html><button></button><input></html>"));
    }
    ```
18. Run it! Should fail!
19. How to fix __add()__ method? We want to make it general for any kind of element. 
What we can do is to create __Element__ interface with __print()__ method as it's common for all tags.
    ```java
    public interface Element {
      String print();
    }
    ```
20. All Tags should implement it.  
   
    ```java
    public class Button implements Element {
    ...
    ```
    
    ```java
    public class Input implements Element {
    ...
    ```
    
    ```java
    public class Html implements Element {
    ...
    ```
21. Change `add()` method accordingly 
    ```java
    private List<Element> elements;
    
    public Element add(Element element) {
      this.elements.add(element);
      return this;
    }
    ```
22. Change `print()` method inside `Html`.
    ```java
    print String print() {
      StringBuilder result = new StringBuilder().append("<html>");
      
      for (Element element: elements) {
        result.append(element.print());
      }
      
      return result.toString();
    }
    ```
23. Run the test. It should pass!
24. Next step attributes.

    ```java
    @Test
    public void attributes() {
      String html = new Html()
          .add(new Button().addAttr("class", "btn btn-primary"))
          .add(new Input().addAttr("name", "emailField"))
          .print();
    
      assertThat(html, is("<html><button class=\"btn btn-primary\"></button><input></html>"));
    }
    ```
25. Test should fail! Let's fix compilation problems.
26. We are going to add `addAttr(String attrName, String value)` method in all 3 elements.
    ```java
    private Map<String, String> attributes;
    
    public Button addAttr(String attrName, String value) {
      this.attributes.put(attrName, value);
      return this;
    }
    ```
27. Adjust `print()` method accordingly.
    in Button.java  
    ```java
    public String print() {
       StringBuilder result = new StringBuilder().append("<button");
       
       for (String attrName : attributes.keySet()) {
           result.append(' ')
                 .append(attrName)
                 .append("=\"")
                 .append(attributes.get(attrName))
                 .append('"')
       }
       
       return result.append("></button>").toString();
    }
    ```
28. Run the test. It should pass!
29. Let's do some simple Refactoring[^2].
We have a duplicated code in the `print()` methods where we append the attributes.
It can be extracted to abstract super-class.
30. Create `ua.dan_it.htmlbuilder.AbstractElement` class
    ```java
    public abstract class AbstractElement {
      
      // move print() method in here
    }
    ```
31. Change all classes-elements
    ```java
    public class Button extends AbstractElement implements Element {
      
    }
    ```
32. What's wrong with our code now? Try to run. All elements called 'button', because we hard-coded it in AbstractClass.
33. Let's add one more field in Abstract class, which called 'tagName'.
    ```java
    public abstract class AbstractElement implements Element {
      protected String tagName;
      protected Map<String, String> attributes = new HashMap<>();
      
      protected AbstractElement(String tagName) {
          this.tagName = tagName;
      }
      
      public String print() {
        StringBuilder result = new StringBuilder().append('<').append(tagName);
       
        for (String attrName : attributes.keySet()) {
          result.append(' ')
                .append(attrName)
                .append("=\"")
                .append(attributes.get(attrName))
                .append('"')
        }
       
        return result.append("></").append(tagName).append('>').toString();
      }
    }
    ```
    
    ```java
    public class Button extends AbstractElement implements Element {
      
      public Button() {
        super("button");
      }
    }
    ```
34. Now it should work. Run test to check it out!
35. Next. Nested elements.
    ```java
    @Test
    public void nestedElements() {
      String html = new Html()
          .add(new Body()
            .add(new Div()
              .add(new Button()))
            .add(new Input()))
          .print();
    
      assertThat(html, is("<html><body><div><button><button></div><input></input></body></html>"));
    }
    ```
36. Move list of elements from `Html` class to `AbstractElement`. And update `print()` method.
37. Run tests. It should pass!
38. Next test and task is add text value insede tags, like
    ```html
    <html>
      <body>
      Just simple text w/o surrounded tag!
      <button>Text Value!</button>
    </html>
    ```
39. Next test is about CSS. Write down the following code and implement functionality.
    ```java
    @Test
    public void addCssFile() throws IOException {
      String html = new Html()
          .add(new Head()
            .add(new Link()
                .addAttr("href", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css")
                .addAttr("rel", "stylesheet")))
          .add(new Body()
            .add(new Button()
                .addValue("Press Me")
                .addAttr("onclick", "alert('Hello!')")
                .addAttr("class", "btn btn-primary")))
          .print();

      FileWriter writer = new FileWriter("/tmp/test.html");
      writer.write(html);
      writer.close();
    }
    ```
40. Open `/tmp/test.html` in a browser (from comman-lne it's a `$ open /tmp/test.html`). You should see a nice blue button.
41. Press the button. Pop-up window should appear.
42. Little refactoring is left. Let's add factory method[^3] in each tag-class that return a new instance. Spread `new` operator is now a good idea.
    in Button.java
    ```java
    public static button() {
        return new Button();
    }
    ```
    In the test, we can import these methods statically, so then our code will look like
    
    ```java
    @Test
    public void addCssFile() throws IOException {
      String html = html()
          .add(head()
            .add(link()
                .addAttr("href", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css")
                .addAttr("rel", "stylesheet")))
          .add(body()
            .add(button()
                .addValue("Press Me")
                .addAttr("onclick", "alert('Hello!')")
                .addAttr("class", "btn btn-primary")))
          .print();

      FileWriter writer = new FileWriter("/tmp/test.html");
      writer.write(html);
      writer.close();
    }
    ```
    
[^1]: [Classpath](https://docs.oracle.com/javase/tutorial/essential/environment/paths.html)
[^2]: [Refactoring](https://www.amazon.com/Refactoring-Improving-Design-Existing-Code/dp/0201485672/ref=sr_1_1?ie=UTF8&qid=1494158657&sr=8-1&keywords=Refactoring)
[^3]: [Factory Method](https://en.wikipedia.org/wiki/Factory_method_pattern)