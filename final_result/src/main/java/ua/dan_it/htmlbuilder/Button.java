package ua.dan_it.htmlbuilder;


public class Button extends AbstractElement implements Element {

  public Button() {
    super("button");
  }

  public static Button button() {
    return new Button();
  }
}
