package ua.dan_it.htmlbuilder;


public class Head extends AbstractElement implements Element {

  protected Head() {
    super("head");
  }

  public static Head head() {
    return new Head();
  }
}
