package ua.dan_it.htmlbuilder;


public class Link extends AbstractElement implements Element {

  protected Link() {
    super("link");
  }

  public static Link link() {
    return new Link();
  }
}
