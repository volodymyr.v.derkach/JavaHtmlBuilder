package ua.dan_it.htmlbuilder;

import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static ua.dan_it.htmlbuilder.Body.body;
import static ua.dan_it.htmlbuilder.Button.button;
import static ua.dan_it.htmlbuilder.Head.head;
import static ua.dan_it.htmlbuilder.Link.link;

public class HtmlBuilderTest {

  @Test
  public void elements() {
    String html = new Html()
        .add(new Button())
        .add(new Input())
        .print();
    assertThat(html, is("<html><button></button><input></input></html>"));
  }

  @Test
  public void attributes() {
    String html = new Html()
        .add(new Button().addAttr("class", "btn btn-primary"))
        .add(new Input().addAttr("name", "emailField"))
        .print();

    assertThat(html, is("<html><button class=\"btn btn-primary\"></button><input name=\"emailField\"></input></html>"));
  }

  @Test
  public void addCssFile() throws IOException {
    String html = new Html()
        .add(head()
            .add(link()
                .addAttr("href", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css")
                .addAttr("rel", "stylesheet")))
        .add(body()
            .add(button()
                .addValue("Press Me")
                .addAttr("onclick", "alert('Hello!')")
                .addAttr("class", "btn btn-primary")))
        .print();

    FileWriter writer = new FileWriter("/tmp/test.html");
    writer.write(html);
    writer.close();
  }
}
