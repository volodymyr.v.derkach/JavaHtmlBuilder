package ua.dan_it.htmlbuilder;

public class Body extends AbstractElement implements Element {

  protected Body() {
    super("body");
  }

  public static Body body() {
    return new Body();
  }

}
